<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary-1 sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('home')}}">
    <div class="sidebar-brand-icon rotate-n-15">
        <!-- <i class="fas fa-laugh-wink"></i> -->
        <img src="client_asset/assets/dest/loader/loader.png" style="width:60px;height:50px">
    </div>
    <div class="sidebar-brand-text mx-3">Manage <sup>FlowerShopDaLat</sup></div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item active">
    <a class="nav-link" href="admin/home/">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Bảng điều khiển</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Chức năng
</div>
@if(Auth::user()->id_levels == 1)
<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
    <a class="nav-link" href="admin/bill/list-bill">
    <span>Hóa đơn</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="admin/event/list-event">
    <span>Sự kiện</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/sale/sale-of">
    <span>Sự kiện giảm giá</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/category/list-category">
    <span>Danh mục</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/shop/list-shop">
    <span>Cửa hàng</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/product/list-product">
    <span>Sản phẩm</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/coupon/list-coupon">
    <span>Mã giảm giá</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/user/list-user">
    <span>Người dùng</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin/contact/list-contact">
    <span>Phản hồi</span></a>
</li>
@else
    <li class="nav-item">
    <a class="nav-link" href="admin/product/list-product">
    <span>Sản phẩm</span></a>
</li>
@endif


<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
</ul>
<!-- End of Sidebar -->