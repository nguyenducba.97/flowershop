
<!-- Footer Section Begin -->
<footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <a href="#"><img src="client_asset/assets/dest/img/logo_flowershopdalat.png" alt=""></a>
                        </div>
                        <ul>
                            <li>{{__('address')}}: TP. Hà Nội</li>
                            <li>{{__('phone_number')}}: +84 999 999 999</li>
                            <li>{{__('email')}}: quynhkarryshophoatuoi@gmail.com</li>
                        </ul>
                        <div class="footer-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1">
                    <div class="footer-widget">
                        <h5>{{__('infomation')}}</h5>
                        <ul>
                            <li><a href="#">{{__('aboutus')}}</a></li>
                            <li><a href="#">{{__('checkout')}}</a></li>
                            <li><a href="#">{{__('contact')}}</a></li>
                            <li><a href="#">{{__('serivius')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-widget">
                        <h5>{{__('myaccount')}}</h5>
                        <ul>
                            <li><a href="#">{{__('serivius')}}</a></li>
                            <li><a href="#">{{__('contact')}}</a></li>
                            <li><a href="#">{{__('cart')}}</a></li>
                            <li><a href="#">{{__('shop')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="newslatter-item">
                        <h5>{{__('joinournewsletternow')}}</h5>
                        <p>{{__('gete-mail')}}</p>
                        <form action="#" class="subscribe-form">
                            <input type="text" placeholder="Nhập Email">
                            <button type="button">{{__('sbuscribe')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> {{__('all_rights_reserved')}}
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <div class="payment-pic">
                            <img src="client_asset/assets/dest/img/payment-method.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->